Welcome to Hello Hunter Wiki
============================

Integrated Voice Broadcasting and Auto & Predictive Dialing made easy!

Getting Started
---------------

.. toctree::
   predictive-dialer/Quick-Setup-Guide
   voice-broadcasting/Quick-Setup-Guide
  
Overview
--------

.. toctree::
   predictive-dialer/Dashboard
   predictive-dialer/List-View
   predictive-dialer/Contact-View
   predictive-dialer/Predictive-Campaigns
   voice-broadcasting/Voice-Broadcasting
   predictive-dialer/Agent-Monitor
   predictive-dialer/Admin
   predictive-dialer/Settings

FAQs
----

**General**

.. toctree::
   :maxdepth: 2
   
   faqs/Payment-Methods
   faqs/Cannot-login
   faqs/Cannot-Upload-Leads


**Predictive Dialer**

.. toctree::
   :maxdepth: 2
   
   faqs/Calling
   faqs/X-Lite-EyeBeam
   faqs/X-Lite-EyeBeam-Errors
   faqs/POST-URL-Configuration
   faqs/Internet-Connection-Bandwidth
   faqs/Computer-Requirements
   faqs/drop-call-screen-freeze-voice-quality
   faqs/Long-Waiting-Time
   faqs/Unknown-Agent
   faqs/Leads-Gone
   faqs/Customers-Cannot-Hear-Me
   faqs/Agents-Getting-Kicked

**Voice Broadcasting**

.. toctree::
   :maxdepth: 2

   faqs/Do-Not-Call-List
   faqs/VB-will-not-run
   faqs/VB-Configuration
   faqs/Not-Getting-Calls
  