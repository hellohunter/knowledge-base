*********
Dashboard
*********

Below is the overview of the **Dashboard** screen.

.. image:: images/dashboard.png
   :width: 800px

Join Predictive Campaigns
=========================

This is the most important part of the Dashboard. The **Predictive Campaigns** panel lists you an overview of your existing campaigns. Click any campaign to go to its *Contact View* and begin calling.

Learn how to `Add a Campaign <Quick-Setup-Guide.html#create-a-new-campaign>`_ or `Edit a Campaign <Predictive-Campaigns.html#edit-a-campaign>`_.

Search a Contact
================

This feature allows you to go directly to the **Contact View** with the phone number ready to be called. If for example there are multiple records of that phone number from different campaigns, you can choose from the list that will pop up.

Generate Statistics
===================

This generates a report of your call statistics within a chosen timeframe.

You can either choose from the pre set of options in the dropdown:

.. image:: images/stats-dropdown.png
   :width: 400px

Or you can choose a custom timeframe by clicking the *calendar icon* and choosing a date.

.. image:: images/stats-custom.png
   :width: 400px

Callback Schedule
=================

The **Schedule Panel** allows you to call directly the contact by clicking the *eye or phone icon*. Another main feature is editing the callback schedule by clicking the *pencil icon*. You can edit the date and time by the hour even by the minute. Click **Shedule Callback** button when done.

.. image:: images/callback.png
   :width: 400px

Agent Productivity
==================

If for example you want to print or download your statistics, you may click the **Display Agent Productivity** button. Clicking the *print button* will popup the browser's print function, while the *save as CSV button* will export and download your statistics in CSV file format.

.. image:: images/agent-productivity-popup.png
   :width: 600px
