*********
List View
*********

Below is the overview of the **List** view. The feature allows you to filter contacts within a certain disposition. You can also do a manual phone search Where `field name` is equal to `x`.

.. image:: images/list-view.png
   :width: 800px

Learn how to `Configure the List View here <Quick-Setup-Guide.html#configure-the-list-view>`_.