*************
Agent Monitor
*************

Real-Time Monitor
=================

Below is the overview of the **Agent Monitor** screen. You will be able to monitor all outgoing calls. You can also Speak, Barge In or Coach an agents call.

.. image:: images/agent-monitor.png
   :width: 800px

Recorded Calls
==============

You can download recorded calls of agents by going to the **Recorded Calls** tab under Agent Monitor, then click the **View List** button. Click the *download icon* beside the chosen call to being downloading.

.. image:: images/recorded-calls.png
   :width: 800px

Speak, Barge In or Coach an Agents Call
=======================================

Selected users can be given permission to Speak, Barge In or Coach a live call under the Agent Monitor Tab. A couple of items are required:

1. As **SIP / Web Phone Connection** is required for both users of the dialer for the Speak, Barge In or Coach to work. Learn how to `Setup a Voice Connection here <Quick-Setup-Guide.html#specify-a-connection-method>`_.

2. Enable the user's Agent Monitor Tab under **User Settings**.

   1. Go to **Admin**

   2. Edit user settings by clicking the *yelloe pencil icon* under the chosen user.

   3. Check the Enable Monitor checkbox under the Privileges tab then Save changes.

      .. image:: images/privilege.png
         :width: 600px

      .. note:: Do not enable the Agent Monitor tab for the users you want to Speak, Barge In or Coach. This tab should only be enabled for the Managers or Administrator of the dialer. 

Going back to the **Real-Time Monitor** tab under the Agent Monitor screen, click on the *speech ballon (sound icon)* next to an agent to Barge (listen) in.

.. image:: images/agent-monitor.png
   :width: 800px

The dialer will generate a call to your phone for you to accept and listen in on the agent call.

Press ``1`` to speak with their contact.

Press ``2`` to coach your agent without the receiver hearing.

Press ``3`` to conferance in so both your agent and receiver hear you.

.. note:: A user will show as unknown until they have taken their first call after joining a campaign.

