********************
Predictive Campaigns
********************

Below is the overview of the **Campaigns** view.

.. image:: images/campaigns.png
   :width: 800px

Create New Campaign
===================

Learn how to `Create a New Campaign <Quick-Setup-Guide.html#create-a-new-campaign>`_.

Edit a Campaign
===============

For example you want to increase or decrease calling speed *(Dial Rate Override)*, change calling hours, or any settings that you saved when you created the campaign, you may click the *pencil icon* below the campaign and save any changes.

.. image:: images/edit-campaign.png
   :width: 600px

Upload Leads / Contact
======================

Learn how to `Upload Leads <Quick-Setup-Guide.html#upload-contacts-leads>`_.

Remove Contacts
===============

Begin removing contacts by clicking the *trash icon*. There are three options to remove contacts:

``All Contacts``: removes all the contacts from the campaign.

``Bad Numbers``: removes contacts that have been called but not answered.

``Disposition As``: removes contacts within the chosen disposition.

.. image:: images/remove-contacts.png
   :width: 600px

.. note:: To maintain compliance, remove contacts from a campaign does NOT remove the actual contact record. It only removes its reference from the campaign

Transfer contacts from one campaign to another
==============================================

You can transfer contacts from another predictive dialing campaign to the current campaign by clicking the *leaf icon*.

Choose whether to move **All Contacts**, **Bad Numbers** only, or certain contacts **Dispositioned As** the chosen disposition(s). The dispositioned contacts can be limit by date by checking the **Limit by Date** and choosing between two dates.

.. image:: images/recyle-contacts.png
   :width: 600px

Add Campaign Message
====================

Campaign Message is an **Abandon Message** that can be played if there is no agent available to answer the call. Add Message by clicking the *sound icon*.

1. Click **Add Sound File**

2. Enter the required **Name**, Description.

3. Select method:
  
   ``Uploading .WAV file``: Click **Select WAV File** button.

   ``Record Over the Phone``: Choose the number you wish the dialer to call you. When dialed and prompted, record your message over the phone.

   .. image:: images/add-sound-file.png
      :width: 600px

4. After adding the Sound, the sound file will be listed under **Sound Files Available**. Click **Set As Abandoned Message** beside the chosen sound file. Then click **Copy Sound Files to Campaign** button.

   .. image:: images/upload-sound.png
      :width: 600px

Export Calls / Contact list of a campaign
=========================================

Export a list of calls made for the current campaign **(Call Detailed Records)**, or a list of all contacts with chosen dispositions and / or status **(Contacts List)** directly to your email. Export by clicking *export icon with arrow pointing upwards*.

.. image:: images/export-contacts.png
   :width: 600px

Display Graph Stats
===================

Display statistics by clicking the *bar graph icon* at the end of the icon groups.

Listen to Recorded Messages
===========================

To listen the campaign recordings you can click on the *recording link* labeled as **Recordings** under the statistics of the campaign. You can view the recordings in a web page or by downloading a csv file with the recording links, all recording are saved for a period of 30 days.
