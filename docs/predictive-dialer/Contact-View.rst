************
Contact View
************

Below you will see the **Contact View**. This is the main screen you will be working with mostly. The title on top is where you can tell what campaign you are dialing.

.. image:: images/contact-view.png
   :width: 800px

Contact Details
===============

This is where the details of your contact populates once you made contact. To edit their details, simply click the field to highlight it. You can now start typing new data.

Control Bar
===========

.. image:: images/control-bar.png
   :width: 600px

``Connection`` status: This area indicates the connection to your phone and to your contact.

``Edit`` *pencil icon*: Let's you edit the phone number of the current contact.

``Recent Calls`` *list icon*: Lists your connected calls in as maximum as 3 days.

``POST to URL`` *circled arrow icon*: When you get a Qualified or Good Lead you can click this and it will post/create a new lead into your CRM. More information about POST URL configuration `here <POST-URL-Configuration>`_.

``Manual Dial`` button: Let's you dial in a direct call mode like making a manual phone call.

``Agent Disconnect`` button: This is used to disconnect or terminate connection to your phone like Web Phone, SIP Phone, etc. You should never pick up or hang up from this phone. It is only used to dial extensions.

``Call Mode`` dropdown: Let's you switch to other predictive campaigns or change to direct dial mode.

Call Controls
^^^^^^^^^^^^^

``Get Contact`` *green phone icon*: Make a call. If you click the get contact button, you will see the waiting status, which means the software is locating a contact. When you hear their ringback sound, and the status changes to Talking, it means a contact has been found.

``Hang up`` *red phone icon*: Hang up from the call. A call may go on for 5 seconds before you get a call transfer, and it may go on for 30 seconds. You can just hang up and click get contact button again if it goes on for more than 30 seconds. There could be a problem locating a business for you to speak with. The dialer senses that the numbers have been disconnected or it's a voicemail.

``Play Message`` *sound icon*: Plays a pre-recorded messages to be left on answering machines or a live message while on a call.

``Record`` *icon*: Let's you record your conversation once you are connected.

``3-Way`` *person icon*: Allows you to add a 3rd person to your conversation.

``Send Email`` *envelop icon*: Sends email with the chosen template to your contact.

``Previous & Next`` *icon*: Allows you to move back to the previous call or move on to the next.

Disposition Bar
===============

.. image:: images/disposition-bar.png
   :width: 600px

``Show Script`` *file icon*: If you ever get stucked on what to say, you can choose to show a script by clicking this button. You can choose to automatically open script once you are connected by checking the box `Popup scrip on connect`.

``Upon disposition, load next call``: You can choose

Every call **must have a disposition** if you made contact. You can choose from the dropdowns in the disposition bar to tell what happened during the call.

For example you **Made Contact**, you can choose whether you hit your goal, the contact is not interested, or when they requested to not be your our list. Or if you are **Unable to Connect**, you can choose if it's because it's a wrong number, a voicemail, or if the line is busy or not available.

You could **Schedule Callback** for the current call by choosing the time from this dropdown:

.. image:: images/callback.png
   :width: 400px

You could also **create a custom disposition**.

1. Go to **Admin** by clickin the *person icon* at the bottom of the navigation area.

2. Under **Setup** panel, click **Configure Custom Disposition**.

3. Enter a name for the custom disposition then click the **Add** button.

   .. image:: images/custom-dispositions.png
      :width: 600px

**Assigning** the contact to a specific agent is also possible by choosing the name of the agent from the `Assign to` dropdown.

Contact Log
===========

In this table, you will see all calls history you made for the current contact with it's disposition message.

.. image:: images/contact-log.png
   :width: 600px

Notes
=====

Adding notes is important if you have gotten any additional vital information. This will be useful especially when you start scheduling follow-up.

.. image:: images/add-note.png
   :width: 600px
