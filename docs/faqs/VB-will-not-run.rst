*******************************
My Voice Broadcast will not run
*******************************

.. image:: ../voice-broadcasting/images/vb.png
   :width: 800px

When a voice broadcast does not run when you click run the **Start** button, please check your ``Do not call before`` and ``Do not call after`` time settings for that capaign. 

If the current time do fall in this configured window then the campaign will not run. Please wait until the configured time or change your campaign settings. 

Also please check to see if there are remaining contacts in your list. If there are no more available contacts then the dailer will not run. 