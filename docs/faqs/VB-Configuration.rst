****************************************************************
How can I view the configuration of my Voice Broadcast campaign?
****************************************************************

The View Detailed Description interface can be accessed by clicking the *magnifying glass icon* under a campaign.

.. image:: images/campaign-magnify.png
   :width: 600px

This will give you access to all configuration settings and statistics about your campaign. You are also able to access any voicemails that have been left on the system or recorded calls. You are also able to download messages that are currently used as the broadcast message.

.. image:: images/campaign-details.png
   :width: 600px