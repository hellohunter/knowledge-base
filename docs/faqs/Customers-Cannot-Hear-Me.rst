*******************************
Why can't my customers hear me?
*******************************

If you are using a soft phone, like X-Lite, your leads may not be able to hear you for obvious reasons. For example, your PC speakers may be turned down too low, or on mute. Check to see if your speakers are turned down.

If that doesn't resolve your issue, you may want to look at your X-Lite device settings.

First, check to see if your X-Lite has been on mute.

.. image:: images/mute-xlite.png
   :width: 400px

If that doesn't help go to **Preferences**. A new window will pop up, click on the "Devices" tab.

.. image:: images/xlite-preferences.png
   :width: 600px

Under the Headset area, click on the dropdowns and select your headset for Speaker Device and Microphone Device.

Once those have been selected, click on Apply, then OK.

If your leads still can not hear you, be sure to check and see if you have a mute button on your USB headset that you may have pressed by accident.

If after these steps, either you can not be heard or you can not hear the lead, please call our support team at ``support@hellohunter.com`` for assistance.
