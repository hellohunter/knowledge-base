**********************
POST URL Configuration
**********************

Posting Qualified Leads into the CRM
====================================

You have purchased **_leads_** or **_data_** from a third party or you may already have the data which is typically **UNQUALIFIED**. Now you want to dial through this data and identify/capture the **_Good Leads_** or **_Qualified prospects_**. So you would load your data/leads into the dialer and when a *Good Lead* or *Qualified Prospect* is reached, you would post this Lead into your CRM from the dialer. Once it is in your CRM you can subject it to all of your marketing channels, work flows, etc.

Step 1
------

Contact your CRM company and get posting instructions for **Posting Leads** through the URL. Also known as a **Web Get**. They will send you a URL that is tailed to your company and their fields that you can post to. Make sure you get both.

**Example:**

```
http: //yourcompany.leadtrac.ws/WebPost/import.aspx/?
```

**FIELDS** : Company, HomeNumber, FirstName, LastName, AddressLine1, City, State, BusinessLegalName

Step 2
------

Load your data/leads into the dialer. See how to `Upload leads here <Quick-Setup-Guide.html#upload-contacts-leads>`_. Make sure you have corresponding fields for the data you want to post into your CRM.

**Example:**

**LoanAmount** field in the dialer will post over to the **LoanAmount** field in yourCRM. 

Step 3
------

Create your Custom String Take the Posting URL given to you by your CRM company
 
```
https://www.leadmailbox.com/post.aspx?account=xxxx&amp;source=HH&amp;
```

**NOTE** : Their string may sometimes just stop at the ? but other times they may have some variables in it to validate the source. For example: ``source=blahblah&amp;``
 
Map the fields in the dialer with the CRM Fields. Dialer fields will have ``#{ }`` around it so it passes the information in those fields not the label. 
 
The URL that you create should look like the example below.
 
```
https://www.leadmailbox.com/post.aspx?account=xxxx&amp;source=HH&amp;Phone=#{Phone}&amp;FirstName=#{FirstName}&amp;LastName=#{LastName}&amp;Prop_Street=#{Address}&amp;Prop_City=#{City}&amp;Prop_State=#{State}&amp;Prop_Zip=#{Zip}&amp;Email=#{Email}&amp;CelPhone=#{CelPhone}
```

Step 4
------

Add this Custom String to the dialer.

1. Go to **Admin**.

2. Go to **Edit properties** by clicking the *pencil icon* under the user(s) you wish to have this ability. Then add this URL String to the **Post to URL** field and hit save.

   .. image:: images/edit-user.png
      :width: 600px
 
Step 5
------

Now when you get a Qualified or Good Lead you can click the **Post to URL** icon on the Contact View and it will post/create a new lead into your CRM.

.. image:: images/post-to-url.png
   :width: 600px

