************************************
Why can't I upload leads / contacts?
************************************

If you are having trouble uploading contacts, this is most likely caused by your spreadsheet not being saved in CSV Format.

1. Open your Excel file.

2. Click on **File** then **Save As**.

3. Under File Format, choose **Comma Separated Values** *(csv)*.

.. image:: images/csv.png
   :width: 600px

4. Upload contacts as usual:

   1. Upload contacts by clicking the *plus* icon under the newly created Campaign.

   2. Follow the onscreen instructions:
      
      1. Select a CSV file by pressing **Load CSV File** button. 

         .. note:: Make sure that your CSV file have column names that match the field names that you are using.

         As a minimum you should have a column named phone in your CSV file. If you do not have this column named in the CSV file you will get import errors. 

         Should your database fields happen to match exactly how they are displayed in your spreadsheet, then you should click the **Auto Assign** button, and our dialer will automatically complete this process for you.

      2. Select the appropriate field for the columns.

      3. Once you have all the database fields all correctly associated, proceed to the check boxes.
          
         ``Randomize`` *leave checked*: The Dialer will randomize the order in which the contacts are called in.
           
         ``Prevent Duplicate Phone Number in File``: Duplicate numbers will be automatically scrubbed.
           
         ``Prevent Duplicate Phone Numbers in Campaign``: Check and scrub duplicate numbers in the campaign.
           
         ``Scrub Against Our Do-Not-Call List``: Checking this box will remove Federal Do Not Call numbers from your list.
           
         Finally, always check the ``I am Allowed to Call these Contacts``. Otherwise, you won't be able to upload the contacts.

      4. When complete, press Upload.

      .. image:: ../predictive-dialer/images/upload-contacts.png
          :width: 600px

   3. Once uploaded, press the green *refresh icon* under the newly created **Campaign** to confirm your ``.CSV`` has been imported correctly. You will receive an email from the system when import is complete. Should you have any issues with this import please review the email.