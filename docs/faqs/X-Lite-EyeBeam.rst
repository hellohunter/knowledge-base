***********************
X-Lite or EyeBeam Setup
***********************

Here at HelloHunter we often get asked by new customers (and sometimes by existing customers) **“Which is the best Sip Phone to use with your system?”**. Although there is a host of options available we tend to only recommend two. Firstly is X-lite, while this is perfectly adequate for the job, and is pretty similar to our second choice (Eyebeam), overall we prefer to use Eyebeam ourselves, and for that reason recommend our customers may also like to do so. Eyebeam can be downloaded free of charge via the internet, and we can provide a license free of charge to all our customers if needed. This can be obtained by our customers by emailing: ``support@hellohunter.com``. Once you have downloaded either of our two preferred options you will need to configure it by following these simple instructions:

1. Download X-lite or Eyebeam and install it. (if Eyebeam and you have obtained a license from us, please insert it when prompted)

2. Open your Predictive Dialer account with Hello Hunter and log in.

3. Select the Admin tab from the menu bar, and click on the blue icon located near the middle of the screen about a third of the way down, labeled Edit Connection Method.

4. Next select the second option labeled: Sip Phone – software or hardware like X-lite or Zoiper. (you may now wish to take a note of the information labeled, Display Name/Auth User Name/Username, Password and Domain. Although the same information is also located under the settings tab on the Hello Hunter screen in the bottom left hand corner). Click Save Changes.

5. Now, open X-lite or Eyebeam and click on the top left tab. Find and click on “Sip Account Settings”. Then proceed to fill in the fields with the relevant information, Display Name, User Name, Password, Authorization User Name and Domain.

6. Under the Domain Proxy part be sure that Register with Domain and receive incoming calls is checked, and that Target Domain is selected. Then press Apply and OK.

7. Next you should see on your Sip Phone, Your username is Sip_****. This means it is configured correctly.

8. As a final check that everything is configured correctly, Click on your Settings Tab on the Hello Hunter screen, and click on: Test Sip Phone Registration and Setup. (This is located near the center of the screen, 2 thirds of the way down in blue writing).

If everything is configured correctly, your phone will ring, a phone icon will pop up in the bottom right corner, and when you click on the green phone icon you will hear a recorded message.

If for any reason this doesn’t happen, please go back and check all your settings again. If you still can’t find the route of the problem then you can always email us at: support@hellohunter.com.