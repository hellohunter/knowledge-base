*******************************************************************
I am experiencing drop calls , poor voice quality or screen freezes
*******************************************************************

Browser
=======

We recommend using Google Chrome which can be downloaded `here <https://www.google.com/intl/en/chrome/browser/>`_. Please ensure that you have the latest version of flash which can be downloaded `here <http://get.adobe.com/flashplayer/>`_

Internet Connection Bandwidth
=============================

See `Internet Connection Bandwidth article <Internet Connection Bandwidth>`_.

Connection Type
===============

We recommend to always use a WIRED (not wi-fi) always-on connection like Fiber or Broadband Cable. In certain situations where the internet connection is being shared it might be necessary to implement VoIP QoS on your router (please consult your network administrator for further instructions).

Dial In Method:  If you are using the call in method you are at the mercy of the telephone carries connecting your call to our servers. This type of call quality can vary based on the calls your local telephone carrier may be experiencing.

SIP/Webphone Connection: 80% of voice quality issues are the result of the end user's internet connection. High quality voice conversations require an extremely consistent (low jitter) connection to our servers. To isolate possible problems, we recommend network administrators download **WinMTR** from `their site <http://winmtr.net/download-winmtr/>`_ then run it against the server you are connecting for *at least an hour*.

Computer Requirements
=====================

**PC Computer** requirements:

``Minimum RAM``:  2GB with 500MB Available

``Minimum Processor``: Intel Pentium 4 , 1.5 GHz Processor

``Optimal RAM``:  3GB with 500MB Available

``Optimal Processor``:Intel Pentium 4 , Any Dual Core Processor

**Mac OSX** requirements:

``Minimum RAM``: 2GB Minimum

``Processor``: 1.5GHz Processor

``Optimal RAM``:  4GB

``Optimal Processor``:  2.66 I7 GHz Processor