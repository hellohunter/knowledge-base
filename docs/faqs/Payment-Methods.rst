*****************************
What are the payment options?
*****************************

We have a variety of payment options for your convenience.

1. Paypal

2. Bank Deposit / Transfer

3. Paying via website by card

   `See our Predictive Dialer plans <http://www.hellohunter.com/predictive-dialer.html#pd-plans>`_

   `See our Voice Broadcasting plans <http://www.hellohunter.com/voice-broadcasting.html#vb-plans>`_

4. Bitcoin