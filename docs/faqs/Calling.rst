*************************************************
How to determine if the Dialer is calling leads?
*************************************************

After clicking **Get Contact** button, take note of the status beside the Campaign's title.

If the dialer is finding leads, the status should be **waiting**. If the dialer found a lead, the status **waiting** should now be accompanied with the contact's phone number.

.. image:: ../predictive-dialer/images/waiting-webphone.png
   :width: 600px

If the dialer made contact, the status will change to **talking**.

.. image:: ../predictive-dialer/images/talking.png
  :width: 600px