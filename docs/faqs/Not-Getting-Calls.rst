*************************************
Why am I not getting calls coming in?
*************************************

The frequency of incoming calls generated via your ``press 1`` voice broadcasting campaign can be based on many factors some of which are not related directly to our systems. These include things like the time you are calling, the quality and suitability of your lists and the broadcast message you are using. Some days may generate more transfers and leads then others and this is to be expected. However if you feel that you are not getting transfers coming in there are a few things we would ask you to try before opening a support ticket. 

1. Use the real time monitor to see what is happening to the calls in the campaign. You should be able to see calls that are being answered, transferred, requested to be put on the DNC list and also the DTMFs that have been pressed by the contact. This will allow you to verify if calls are indeed going out of the system correctly and that transfers are occurring. 

   .. image:: ../voice-broadcasting/images/vb-call-log.png
      :width: 600px

2. Make a test call for the campaign to your cell or other phone. If you are able to hear your broadcast message and are transferred to your agent via ``pressing 1`` with good audio then your campaign is working fine. If this is the case try to increase your dial rate to increase the frequency of inbound transfers. There is little our support engineers can do to assist in this situation as the system is functioning correctly . 

3. If the transfer does not work please try to call your transfer number from your cell or other phone. Some problems with your inbound number provider can prevent calls coming in. If you are able to get through to this number then please open a support ticket with the details of your ``test call, server and phone used for testing number``.  If you are unable to contact your transfer number from a different phone there is some issue with your provider and we would suggest contacting them first.