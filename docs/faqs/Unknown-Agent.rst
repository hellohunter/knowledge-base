**********************************************************
Why do agents show up as unknown in the Agent Monitor tab?
**********************************************************

If your agents are showing up as a status of "unknown" this is because they have not taken a call yet. Once they have taken their first call the status will then be updated going forward with their name and stats.