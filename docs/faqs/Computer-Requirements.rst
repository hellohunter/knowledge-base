****************************************************
What are the technical requirements for my computer?
****************************************************

**PC Computer** requirements:

``Minimum RAM``:  2GB with 500MB Available

``Minimum Processor``: Intel Pentium 4 , 1.5 GHz Processor

``Optimal RAM``:  3GB with 500MB Available

``Optimal Processor``:Intel Pentium 4 , Any Dual Core Processor

**Mac OSX** requirements:

``Minimum RAM``: 2GB Minimum

``Processor``: 1.5GHz Processor

``Optimal RAM``:  4GB

``Optimal Processor``:  2.66 I7 GHz Processor