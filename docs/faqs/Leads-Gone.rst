**********************
Why are my leads gone?
**********************

You may find that as you log in as an Admin and you go to the Campaigns Tab, that suddenly your leads are gone.

If you see that your lead count for all of your campaigns is at zero, don't panic, they are not gone. Simply click the *refresh icon* under the Campaign. Then you will then be able to see the lead count again.

.. image:: ../predictive-dialer/images/campaigns.png
   :width: 600px
