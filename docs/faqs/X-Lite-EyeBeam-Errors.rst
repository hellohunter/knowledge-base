************************
X-Lite or EyeBeam Errors
************************

404 Registration Error
======================

This error normally occurs when the information imputed into you sip account settings is not correct. You will need to go back and check through your sip account settings to try and locate the mistake and correct it. See `X-Lite or EyeBeam Setup <X-Lite-EyeBeam>`_.

408 – Request Timed Out
=======================

Should you see a 408 error from x-lite or EyeBeam as a rule this means you are not getting any response from the sip registration server which you are trying to connect to with. There are multiple reasons why this may occur but this may be caused by:

1. Your SIP Settings are incorrect
2. Your router is not configured correctly or may not support this application
3. A firewall may be blocking this from working
4. Anti-spyware is blocking this from working

You can try disabling any firewall software you have running on your desktop temporarily, including Windows Firewall. The same also applies for Anti-spyware.
It is also advisable to try connecting directly to your DSL/cable modem and bypassing your router this will determine if the router is causing any problems.

Error 500 Message
=================

If you are using Xlite or EyeBeam and while trying to log in, you get the error 500 message this probably means that your ISP (Internet Service Provider) does not have the ability to facilitate VOIP calls over their network.
You can contact your Internet Service Provide and ask them to assist you with making the correct changes needed to your router. Hopefully these changes will facilitate the working of VOIP services.

Should any of these errors still reoccur after taking the steps listed above, please feel free to email us at support@hellohunter.com.
