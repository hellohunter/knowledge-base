****************************************
Why am I waiting a long time for a call?
****************************************

*This can be due to a couple of reasons:*

1. You may have your dial rates set too slow. To increase or decrease calling speed *(Dial Rate Override)* click the *pencil icon* below the campaign. Then change the **Dial Rate Override** field.

   .. image:: ../predictive-dialer/images/edit-campaign.png
      :width: 600px

2. Check if the connection status are both to be sure the system is working and dialing numbers.

3. Check and make sure you have good accurate data.

4. Try using local numbers rather than toll free numbers.

.. note:: We do not suggest using the Answering Machine Detection (AMD) when calling businesses. This is due to the fact that most businesses answer the phone with an introduction rather than "Hello" confusing an answering machine pick up. The AMD feature works best when calling consumers who answer with and expected "Hello" then pausing for a response.