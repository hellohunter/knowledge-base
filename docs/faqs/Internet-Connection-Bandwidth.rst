*****************************
Internet Connection Bandwidth
*****************************

*Internet Connection Bandwidth FAQ*

We have put this document together to provide you with an overview of the **various** problems that you may experience with **_your voip connection_** to our dialer and the tools that are available for helping to troubleshoot these issues. It is our hope that you will have more of an understanding of how we are trying to help you when we ask you to run certain tools.

Voice Quality Issues
====================

If you are experiencing voice quality issues or you want to know if a certain networked location *(such as a new customer site)* will be suitable for running the dialer from, we recommend running the `VOIP test tool <http://myspeed.visualware.com/indexvoip.php>`_ then select the *USA* on the map and then choose *any testing server*. Doing this will allow us to see the speed of your connection and establish if your connection is suitable for voip. An alternative tool that works the same way is `Megapath <http://www.megapath.com/speedtestplus/>`_.

Unstable connections to our servers
===================================

Sometimes an unstable connection from **_your computer_** to our servers can have a big impact on the calls you are placing and general dialer usage. If you are experiencing *cut of phone calls, connection to the dialer drops or something not quite right with the network* every so often you can run ``trace route`` or ``win mtr`` to the server that you are using. This will verify if there is an issue with connectivity and where exactly it is occurring.

**Traceroute** is easy to run as it does not require any downloading of software. However, it is limited in its use as can only be run once. This will give a very time specific view of the connection  to our servers.

We recommend **Win MTR** as it can be run for long periods of time capturing results which can help to track down the issue. To run ``win MTR`` download it from `their site <http://winmtr.net/download-winmtr/>`_ and then run it against the server you are connecting for *at least an hour*.  Please note that your server name is different to the URL that you use to access the dialer. If your URL is ``fra43.hellohunter.com`` for example, your server name is ``fra-43.hellohunter.com``. Please notice the dash used.

Bandwidth
=========

We do have a bandwidth test that can be used to establish the bandwidth of your connection which can be useful for determining if you have enough bandwidth for large amounts of agents. However as this does not take connection quality information into account it is of limited use. This can be run at `speedtest.net <http://www.speedtest.net/>`_.