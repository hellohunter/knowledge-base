*********************************
Why are my agents getting kicked?
*********************************

If your agents are waiting a while between calls, and then suddenly notice that they are logged out, this may be because your settings are telling the dialer to kick idle agents off the dialer. This happens when there is an extended period of time between live answers. The setting disconnects the agent from the voice connection, causing them to have to log out of the dialer and their softphone, and then restarting both.

You can remove this option so that your agents are not kicked off of the dialer, even if there is a long period of time between live answers.

To do this, you must be logged in as the Admin of the account.

1. Go to **Admin** by clicking the *person icon* at the bottom of the navigation area.

2. Click the *yellow pencil icon* to Edit the properties of the agent.

3. Under **Privileges** tab uncheck the **Kick Idle Agents** option.

   .. image:: ../predictive-dialer/images/privilege.png
      :width: 600px

4. Have your agent log out and log back in for the settings to take effect.

Now your agents will not be kicked off of the dialer, even if there is a very long period of time between live answers.