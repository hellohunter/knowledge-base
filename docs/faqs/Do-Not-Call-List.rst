********************************************
How can I add numbers to My Do Not Call List
********************************************

Numbers can be added to the Do Not Call List through the Manage Do Not Call List button under the Setup panel in the main Voice Broadcasting screen. There are two ways that numbers can be added to our system, using a CSV file to upload a group of numbers or add numbers one by one.

Contacts need to be in a CSV format. Our systems do not support other types of contact files. We recommend using Excel to save the contact file as a CSV file.

Please ensure that you have the word "Phone" as name of the field that contains numbers. This is very important. Please do not use PHONE or any other variant. It is required to be Phone ( A capital P followed by lower case one) and specifying this differently will lead to upload errors. 

To upload a group of numbers from a pre created CSV file click browse on the Manage Do Not call list Interface. Locate the your CSV file and then click upload. Click OK to the notification and then click Done to close the interface. You will receive an email with details of the import once complete. Please review this and should you have any issues please contact ``support@hellohunter.com``.

To upload individual numbers to the DNC list add them into the text box in the Manage Do Not Call List interface, using one line for one number. Click Add to DNC. Click OK to acknowledge any notifications and click Done.

.. image:: ../voice-broadcasting/images/dnc.png
   :width: 400px
